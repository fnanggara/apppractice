package com.rf_4210171024.landingpage;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button btnLogin;
    EditText emailText;

    private int REQUEST_CODE = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnLogin = findViewById(R.id.Login_Button);
        btnLogin.setOnClickListener(this);

        emailText = findViewById(R.id.emailText);
    }

    @Override
    public void onClick(View v){
        switch (v.getId()){
            case R.id.Login_Button:
                Intent moveToGraphics;
                moveToGraphics = new Intent(MainActivity.this, GraphicsActivity.class);
                startActivity(moveToGraphics);
                break;
        }
    }

    public void moveToRegister(View view) {
        Intent moveToReg = new Intent(MainActivity.this, RegisterActivity.class);
        startActivityForResult(moveToReg, REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE){
            if (resultCode == RegisterActivity.RESULT_CODE){
                String textVal = data.getStringExtra(RegisterActivity.EMAIL_TEXT);
                emailText.setText(textVal);
            }
        }

    }

}
